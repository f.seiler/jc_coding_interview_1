import os
import threading
import logging
import time
import signal
import sys
import time
import random
# Configure logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')


keep_running = True
with open("./errors.txt", "r") as f:
    error_messages = f.readlines()

def signal_handler(sig, frame):
    """Handles incoming signals to gracefully shutdown."""
    global keep_running
    keep_running = False
    logging.info("Received shutdown signal")

signal.signal(signal.SIGINT, signal_handler)
signal.signal(signal.SIGTERM, signal_handler)

class LogWriter(threading.Thread):
    """A thread that logs a message to a specified file path."""

    def __init__(self, file_path: str):
        super().__init__(daemon=True)
        self.keep_running = True
        self.file_path = file_path
        self.file_stream = open(file_path, "w")

    def stop(self):
        """Signals the thread to stop."""
        self.keep_running = False

    def run(self):
        """Thread execution starts here."""
        try:
            while self.keep_running:
                time.sleep(random.randint(1,5))    
                random_level = random.random()
                if random_level < 0.05:
                    level = "CRITICAL"
                elif random_level < 0.2:
                    level = "WARNING"
                else:
                    level = "INFO"
                message = error_messages[random.randint(0,len(error_messages))]
                self.file_stream.write(f"{int(time.time())} - {level} - {message.strip()}\n")
                self.file_stream.flush()
                os.fsync(self.file_stream.fileno())
            logging.info(f"Stopping {self.file_path}")
        except Exception as e:
            logging.error(f"Error in LogWriter: {e}")
        finally:
            if self.file_stream and not self.file_stream.closed:
                self.file_stream.close()
if __name__ == "__main__":
    try:
        num_logs = int(os.getenv("JC_NUM_LOGS", 5))
    except ValueError:
        logging.error("JC_NUM_LOGS environment variable must be an integer")
        sys.exit(1)

    writers = [LogWriter(f"./jc_log_{log_id}.log") for log_id in range(num_logs)]

    for writer in writers:
        writer.start()

    while keep_running:
        time.sleep(1)  # To prevent a busy-wait loop

    logging.info("Stopping all log writers...")
    for writer in writers:
        writer.stop()
    for writer in writers:
        writer.join()  # Ensure all threads have finished before exiting
    logging.info("All log writers stopped. Exiting program.")