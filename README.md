# Concurrency and Real-time Processing

Problem Statement: Design and implement a multi-threaded logging system that can efficiently handle logs coming from multiple sources simultaneously. The system should be able to:

Process and store logs (in memory) without significant delays.
Handle high throughput, maintaining thread safety.
Provide a mechanism to query logs based on the following attributes (stdin, API, ...):
- Log Level
- String matching
- Timestamp 

You will be given a script called `generate_logs.py`. You can run this script using python
```bash
python generate_logs.py
```

This script will create the following log files and append data to them at random intervals: jc_log_[0-4].log

The log format of these files is as follows `{UNIX_TIMESTAMP} - {LOG_LEVEL} - {MESSAGE}` where LOG_LEVEL is one of ["INFO", "WARNING", "CRITICAL"]

Grab incoming log messages in real-time, process them and provide a mechanism to query these messages (as described above).